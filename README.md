# React Native Intro Project

O objetivo é criar um app React Native simples que lista os vencedores das copas do mundo da FIFA ⚽️🥳

Existe uma API publicada na seguinte URL https://frozen-peak-68797.herokuapp.com/

Os endpoints desta API são:

* **/login**
* **/winners**

O endpoint para a listagem dos vencedores é **/winners**, porém é necessário fazer autenticação através do endpoint **/login**

O endpoint de autenticação espera um método POST com chaves email e password em formato JSON.

Para simplificar, existe um usuário já criado pra você. Abaixo segue o exemplo do formato e dos dados a serem utilizados:

`{"email": "foo@bar.com", "password": "secret"}`

Um exemplo de chamada utilizando curl:

```bash
curl -X POST "https://frozen-peak-68797.herokuapp.com/login"  \
    -H "accept: application/json" \
    -H "Content-Type: application/json"  \
    -d "{\"email\":\"foo@bar.com\",\"password\":\"secret\"}"
```

Após autenticação com sucesso, é retornado um JSON com um Token (JWT), no formato:

`{"Token": "..."}`

O endpoint **/winners** espera que este token seja enviado no do _Header_ de request _Authorization_, seguindo o seguinte formato:

`Authorization: Bearer <token>`

Um exemplo de chamada autenticada utilizando curl:

```bash
curl -X GET "https://frozen-peak-68797.herokuapp.com/winners" \
    -H "accept: application/json" \
    -H "Authorization: Bearer <token-aqui>"
```

Ao final do desenvolvimento, publique o código em algum repositório git (github, gitlab, etc) e nos envie as instruções necessárias para execução.

Algumas observações:

* Trate esta feature como se fosse o primeiro ciclo de entrega para um cliente em um novo projeto.

* Utilize boas práticas de programação e organização de projeto RN/JavaScript, mas não “overthink” e evite “over-engineer”. Lembre-se, é apenas a primeira entrega e o código irá naturalmente evoluir.

* Pense em boas práticas de UI/UX, usabilidade e tratamento de erros (o que acontece se a senha estiver errada ? como a interface se comporta enquanto aguarda a chamada à API?)

* Fique a vontade para utilizar o design e componentes que preferir - Como sugestão, já utilizamos https://reactnativeelements.com/ internamente, mas não sinta-se obrigado a utilizá-la.

* Para facilitar, fique a vontade para desenvolver esta feature utilizando o https://expo.io/.

Divirta-se!



